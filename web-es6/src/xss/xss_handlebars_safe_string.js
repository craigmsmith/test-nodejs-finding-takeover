import Handlebars from "handlebars";
import express from "express";

const router = express.Router();

// http://localhost:3000/xss/handlebars/safe-string/1?message=<script>alert('xss')</script>
router.route("/1").get(async (req, res) => {
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-safestring
    var returnObj = new Handlebars.SafeString("<h1>Handlebars safe string</h1>" +  req.query.message)
    res.send(returnObj.string)
});

// http://localhost:3000/xss/handlebars/safe-string/safe/1?message=<script>alert('xss')</script>
router.route("/safe/1").get(async (req, res) => {
    // ok: rules_lgpl_javascript_xss_rule-handlebars-safestring
    var returnObj = new Handlebars.SafeString("<h1>Handlebars safe string</h1>" +  Handlebars.escapeExpression(req.query.message))
    res.send(returnObj.string)
});

export default router;