const hbl = require("handlebars");

module.exports = function (app) {

  // http://localhost:3000/xss/handlebars/safe-string/1?message=%3Cscript%3Ealert(%27xss%27)%3C/script%3E
  app.get("/xss/handlebars/safe-string/1", async (req, res) => {
    // ruleid: rules_lgpl_javascript_xss_rule-handlebars-safestring
    var returnObj = new hbl.SafeString("<h1>Handlebars safe string</h1>" + req.query.message)

    res.send(returnObj.string)
  });

  // http://localhost:3000/xss/handlebars/safe-string/safe/1?message=%3Cscript%3Ealert(%27xss%27)%3C/script%3E
  app.get("/xss/handlebars/safe-string/safe/1", async (req, res) => {
    // ok: rules_lgpl_javascript_xss_rule-handlebars-safestring
    var returnObj = new hbl.SafeString("<h1>Handlebars safe string</h1>" + hbl.escapeExpression(req.query.message))
    res.send(returnObj.string)
  });

  // http://localhost:3000/xss/handlebars/safe-string/safe/2?message=%3Cscript%3Ealert(%27xss%27)%3C/script%3E
  app.get("/xss/handlebars/safe-string/safe/2", async (req, res) => {
    // ok: rules_lgpl_javascript_xss_rule-handlebars-safestring
    var msg = hbl.escapeExpression(req.query.message);
    var returnObj = new hbl.SafeString("<h1>Handlebars safe string</h1>" + msg)
    res.send(returnObj.string)
  });
};
